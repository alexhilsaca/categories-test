﻿using CategoriesWebApp.Data;
using CategoriesWebApp.Models;
using CategoriesWebApp.Models.ViewModels;
using CategoriesWebApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesWebApp.Repositories
{
    public class SubCategoryRepository : ISubCategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public SubCategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Models.SubCategory>> GetByCategory(int idCategory)
        {

            var categories = await _context.SubCategory
                .Where(s => s.Category.Id == idCategory)
                .ToListAsync();

            return categories;
        }

        public async Task<bool> CreateRange(List<SubCategoryViewModel> models, int categoryId)
        {
            try
            {
                var subCategories = new List<Models.SubCategory>();
                foreach (var model in models)
                {
                    var role = await _context.Roles.FindAsync(model.RoleSelected);

                    var subCategory = new SubCategory
                    {
                        Name = model.Name,
                        Action = model.Action,
                        Role = role,
                        CategoryId = categoryId

                    };

                    subCategories.Add(subCategory);

                }


                await _context.SubCategory.AddRangeAsync(subCategories);

                var saved = await _context.SaveChangesAsync();

                return saved > 0;
            }
            catch
            {
                return false;
            }

        }

        public async Task<bool> EditRange(List<SubCategoryViewModel> models, int categoryId)
        {
            try
            {
                var category = await _context.Categories.FindAsync(categoryId);
                var subCategories = new List<SubCategory>();
                foreach (var model in models)
                {
                    var role = await _context.Roles.FindAsync(model.RoleSelected);
                    var subCategory = new SubCategory
                    {
                        Id = model.Id ?? 0,
                        Name = model.Name,
                        CategoryId = categoryId,
                        Action = model.Action,
                        Role = role,
                    };


                    subCategories.Add(subCategory);
                }

                category.SubCategories = subCategories;

                var savedResult = await _context.SaveChangesAsync();


                return savedResult > 0;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteRange(List<int> ids)
        {
            try
            {
                foreach (var id in ids)
                {
                    var category = await _context.Categories.FindAsync(id);
                    _context.Remove(category);
                    var saved = await _context.SaveChangesAsync();


                    if (saved <= 0)
                        return false;
                }

                return true;

            }
            catch
            {
                return false;
            }
        }
    }
}
