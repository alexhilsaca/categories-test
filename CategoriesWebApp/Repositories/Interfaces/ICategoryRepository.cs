﻿using CategoriesWebApp.Models;
using CategoriesWebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoriesWebApp.Repositories.Interfaces
{
    public interface ICategoryRepository
    {
        Task<List<Category>> Get();
        Task<Category> GetById(int id);
        Task<int> Create(CreateCategoryViewModel model);
        Task<bool> Edit(CreateCategoryViewModel model);
        Task<bool> Delete(int id);
    }
}
