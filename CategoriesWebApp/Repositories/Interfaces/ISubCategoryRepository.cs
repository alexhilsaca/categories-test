﻿using CategoriesWebApp.Models.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CategoriesWebApp.Repositories.Interfaces
{
    public interface ISubCategoryRepository
    {
        Task<List<Models.SubCategory>> GetByCategory(int idCategory);
        Task<bool> CreateRange(List<SubCategoryViewModel> models, int categoryId);
        Task<bool> EditRange(List<SubCategoryViewModel> models, int categoryId);
        Task<bool> DeleteRange(List<int> ids);
    }
}
