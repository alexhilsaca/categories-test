﻿using CategoriesWebApp.Data;
using CategoriesWebApp.Models;
using CategoriesWebApp.Models.ViewModels;
using CategoriesWebApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Threading.Tasks;

namespace CategoriesWebApp.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public CategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Category> GetById(int id)
        {

            var category = await _context.Categories
                .Include(c => c.Role)
                .Include(c => c.SubCategories)
                .FirstOrDefaultAsync(r => r.Id == id);
            return category;
        }

        public async Task<List<Category>> Get()
        {

            var categories = await _context.Categories
                .Include(c => c.Role)
                .Include(c => c.SubCategories)
                .ThenInclude(s => s.Role)
                .ToListAsync();
            return categories;
        }

        public async Task<int> Create(CreateCategoryViewModel model)
        {
            try
            {
                var role = await _context.Roles.FindAsync(model.RoleSelected);

                var category = new Models.Category
                {
                    Name = model.Name,
                    Controller = model.Controller,
                    Action = model.Action,
                    Role = role
                };

                await _context.Categories.AddAsync(category);

                var saved = await _context.SaveChangesAsync();

                return saved > 0 ? category.Id : 0;
            }
            catch
            {
                return 0;
            }

        }

        public async Task<bool> Edit(CreateCategoryViewModel model)
        {
            try
            {
                var role = await _context.Roles.FindAsync(model.RoleSelected);
                var category = await _context.Categories
                    .Include(c => c.SubCategories)
                    .FirstOrDefaultAsync(c => c.Id == model.Id);

                category.Name = model.Name;
                category.Controller = model.Controller;
                category.Action = model.Action;
                category.Role = role;


                var saved = await _context.SaveChangesAsync();

                return saved >= 0;
            }
            catch (DbEntityValidationException dbEx)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {

                var category = await _context.Categories.FindAsync(id);
                _context.Remove(category);
                var saved = await _context.SaveChangesAsync();

                return saved > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
