﻿using CategoriesWebApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CategoriesWebApp.ViewComponents
{
    public class SideViewComponent : ViewComponent
    {
        private readonly ICategoryRepository _categoryRepository;

        public SideViewComponent(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categories = await _categoryRepository.Get();

            return View(categories);
        }
    }
}
