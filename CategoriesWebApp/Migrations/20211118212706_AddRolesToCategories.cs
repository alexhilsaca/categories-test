﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CategoriesWebApp.Migrations
{
    public partial class AddRolesToCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RoleId",
                table: "SubCategory",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RoleId",
                table: "Categories",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubCategory_RoleId",
                table: "SubCategory",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_RoleId",
                table: "Categories",
                column: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_AspNetRoles_RoleId",
                table: "Categories",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubCategory_AspNetRoles_RoleId",
                table: "SubCategory",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Categories_AspNetRoles_RoleId",
                table: "Categories");

            migrationBuilder.DropForeignKey(
                name: "FK_SubCategory_AspNetRoles_RoleId",
                table: "SubCategory");

            migrationBuilder.DropIndex(
                name: "IX_SubCategory_RoleId",
                table: "SubCategory");

            migrationBuilder.DropIndex(
                name: "IX_Categories_RoleId",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "SubCategory");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "Categories");
        }
    }
}
