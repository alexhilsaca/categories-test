﻿using CategoriesWebApp.Data;
using CategoriesWebApp.Models;
using CategoriesWebApp.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesWebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly IEmailSender _emailSender;

        public UsersController(UserManager<IdentityUser> userManager, ApplicationDbContext context, IEmailSender emailSender)
        {
            _userManager = userManager;
            _context = context;
            _emailSender = emailSender;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var users = _context.ApplicationUser.ToList();
            var usersViewModel = new List<UserViewModel>();
            foreach (var user in users)
            {
                var roles = await _userManager.GetRolesAsync(user);
                var userViewModel = new UserViewModel
                {
                    Email = user.Email,
                    Name = user.Name,
                    RoleSelected = roles.FirstOrDefault()
                };

                usersViewModel.Add(userViewModel);
            }
            return View(usersViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var roles = await _context.Roles.ToListAsync();
            var registerViewModel = new RegisterViewModel
            {
                Roles = roles.Select(r => new SelectListItem(r.Name, r.Name)).ToList()
            };
            return View(registerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RegisterViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name };
            var result = await _userManager.CreateAsync(user, "defaultPassword2021**");
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, model.RoleSelected);
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { email = user.Email, token },
                    protocol: HttpContext.Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password", $"Please reset password by clicking  <a href=\"{callbackUrl}\">here</a>.");
                return RedirectToAction("CreateSucceeded", "Users", new { email = model.Email });
            }

            AddErrors(result);
            return View(model);


        }


        [HttpGet]
        public async Task<IActionResult> Edit(string email)
        {
            var user = await _context.ApplicationUser.FirstOrDefaultAsync(u => u.Email == email);
            var roleId = (await _context.UserRoles.FirstOrDefaultAsync(u => u.UserId == user.Id)).RoleId;
            var role = await _context.Roles.FindAsync(roleId);
            var roles = await _context.Roles.ToListAsync();
            var model = new RegisterViewModel
            {
                Email = user.Email,
                Name = user.Name,
                Roles = roles.Select(r => new SelectListItem(text: r.Name, value: r.Name)).ToList()
            };

            model.Roles = model.Roles
             .Select(r =>
            {
                if (r.Value == role.Id)
                {
                    r.Selected = true;
                }

                return r;
            }).ToList();

            return View(model);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(RegisterViewModel model)
        {
            var user = await _context.ApplicationUser.FirstOrDefaultAsync(a => a.Email == model.Email);
            var roles = _context.UserRoles.Where(r => r.UserId == user.Id);
            _context.RemoveRange(roles);
            user.Name = model.Name;
            await _context.SaveChangesAsync();
            await _userManager.AddToRoleAsync(user, model.RoleSelected);
            return RedirectToAction("Index");

        }

        [HttpPost]
        public async Task<IActionResult> Delete(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);

            if (user != null)
            {
                await _userManager.DeleteAsync(user);
            }

            return Json(true);

        }


        [HttpGet]
        public IActionResult CreateSucceeded(string email)
        {

            return View("CreateSucceeded", email);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}