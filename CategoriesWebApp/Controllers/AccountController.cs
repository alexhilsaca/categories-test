﻿using CategoriesWebApp.Models;
using CategoriesWebApp.Models.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CategoriesWebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailSender _emailSender;
        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, IEmailSender emailSender, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _roleManager = roleManager;
        }
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = null)
        {

            var adminRoleExists = await _roleManager.RoleExistsAsync("Admin");
            var userRoleExists = await _roleManager.RoleExistsAsync("User");
            var publicRoleExists = await _roleManager.RoleExistsAsync("Public");

            if (!adminRoleExists)
                await _roleManager.CreateAsync(new IdentityRole("Admin"));

            if (!userRoleExists)
                await _roleManager.CreateAsync(new IdentityRole("User"));

            if (!publicRoleExists)
                await _roleManager.CreateAsync(new IdentityRole("Public"));

            var userExists = await _userManager.FindByEmailAsync("admin_categories_test@protonmail.com");
            if (userExists == null)
            {
                var user = new ApplicationUser { UserName = "admin_categories_test@protonmail.com", Email = "admin_categories_test@protonmail.com", Name = "Admin" };
                await _userManager.CreateAsync(user, "Admin123**");

                await _userManager.AddToRoleAsync(user, "Admin");

            }





            ViewData["ReturnUrl"] = returnUrl;
            var registerViewModel = new LoginViewModel();
            return View(registerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            returnUrl ??= Url.Content("~/");
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    return LocalRedirect(returnUrl);

                }

            }

            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            return View(model);

        }


        [HttpGet]
        public IActionResult ForgotPasswordConfirmation()
        {

            return View();
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return RedirectToAction("ForgotPasswordConfirmation");
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { email = user.Email, token },
                    protocol: HttpContext.Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password", $"Please reset password by clicking  <a href=\"{callbackUrl}\">here</a>.");
                return RedirectToAction("ForgotPasswordConfirmation");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult ResetPassword(string email, string token = null)
        {

            if (email == null || token == null)
                return RedirectToAction("Error");

            var model = new ResetPasswordViewModel { Email = email, Token = token };

            return View(model);

        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return RedirectToAction("ResetPasswordConfirmation");
                }

                var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation");
                }
                AddErrors(result);
            }
            return View();
        }

        [HttpGet]
        public IActionResult ResetPasswordConfirmation()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff(RegisterViewModel model)
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(HomeController.Index), "Home");



        }

        [HttpGet]
        public IActionResult Error()
        {

            return View();

        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}