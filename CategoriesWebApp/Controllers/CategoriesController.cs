﻿using CategoriesWebApp.Models.ViewModels;
using CategoriesWebApp.Repositories.Interfaces;
using CategoriesWebApp.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesWebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoriesController : Controller
    {

        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryService _categoryService;
        private readonly RoleManager<IdentityRole> _roleManager;

        public CategoriesController(ICategoryRepository categoryRepository, RoleManager<IdentityRole> roleManager, ICategoryService categoryService)
        {
            _categoryRepository = categoryRepository;
            _roleManager = roleManager;
            _categoryService = categoryService;
        }

        public async Task<IActionResult> Index()
        {
            var categories = await _categoryRepository.Get();
            return View(categories);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var category = new CreateCategoryViewModel();
            var roles = await _roleManager.Roles.ToListAsync();
            category.Roles = roles
                .Select(r => new SelectListItem(r.Name, r.Id))
                .ToList();

            return View(category);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateCategoryViewModel model)
        {


            if (!ModelState.IsValid)
            {
                var roles = await _roleManager.Roles.ToListAsync();
                model.Roles = roles
                    .Select(r => new SelectListItem(r.Name, r.Id))
                    .ToList();
                return View(model);
            }



            await _categoryService.Create(model);

            return RedirectToAction("Index");

        }

        [HttpGet]
        public async Task<IActionResult> CreateSubCategory(int index)
        {
            var category = new CreateCategoryViewModel();
            var roles = await _roleManager.Roles.ToListAsync();
            category.Index = index;
            category.Roles = roles
                .Select(r => new SelectListItem(r.Name, r.Id))
                .ToList();
            return PartialView("_CreateSubCategory", category);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var category = await _categoryRepository.GetById(id);

            var model = new CreateCategoryViewModel
            {
                Name = category.Name,
                Controller = category.Controller,
                Action = category.Action,
                RoleSelected = category.Role.Id,
                SubCategories = new List<SubCategoryViewModel>()

            };

            foreach (var subCategory in category.SubCategories)
            {
                var subCategoryViewModel = new SubCategoryViewModel()
                {
                    Id = subCategory.Id,
                    Name = subCategory.Name,
                    Action = subCategory.Action,
                    Category = model,
                    RoleSelected = subCategory.RoleId
                };
                model.SubCategories.Add(subCategoryViewModel);
            }

            var roles = await _roleManager.Roles.ToListAsync();
            model.Roles = roles
                .Select(r => new SelectListItem(r.Name, r.Id))
                .ToList();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CreateCategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {

                var errors = ModelState.Select(x => x.Value.Errors)
                    .Where(y => y.Count > 0)
                    .ToList();


                var roles = await _roleManager.Roles.ToListAsync();
                model.Roles = roles
                    .Select(r => new SelectListItem(r.Name, r.Id))
                    .ToList();
                return View(model);
            }
            await _categoryService.Edit(model);

            return RedirectToAction("Index");

        }





        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {

            await _categoryService.Delete(id);

            return Json(true);

        }


    }
}