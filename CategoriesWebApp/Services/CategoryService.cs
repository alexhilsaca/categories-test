﻿using CategoriesWebApp.Models.ViewModels;
using CategoriesWebApp.Repositories.Interfaces;
using CategoriesWebApp.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CategoriesWebApp.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ISubCategoryRepository _subCategoryRepository;

        public CategoryService(ICategoryRepository categoryRepository, ISubCategoryRepository subCategoryRepository)
        {
            _categoryRepository = categoryRepository;
            _subCategoryRepository = subCategoryRepository;
        }

        public async Task<bool> Create(CreateCategoryViewModel model)
        {
            try
            {
                var resultCategory = await _categoryRepository.Create(model);
                if (resultCategory == 0)
                    return false;

                bool subCategoriesResult;
                if (model.SubCategories.Count > 0)
                {
                    subCategoriesResult = await _subCategoryRepository
                        .CreateRange(model.SubCategories, resultCategory);
                }
                else
                {
                    return true;
                }


                return subCategoriesResult;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        public async Task<bool> Edit(CreateCategoryViewModel model)
        {
            try
            {
                var categoryResult = await _categoryRepository.Edit(model);
                if (!categoryResult)
                    return false;

                var subCategoriesResult = await _subCategoryRepository.EditRange(model.SubCategories, model.Id);

                return subCategoriesResult;
            }
            catch (Exception exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var category = await _categoryRepository.GetById(id);
                var subCategoriesIds = category.SubCategories.Select(s => s.Id).ToList();
                var subCategoriesResult = await _subCategoryRepository.DeleteRange(subCategoriesIds);

                if (!subCategoriesResult) return false;

                var categoryResult = await _categoryRepository.Delete(category.Id);

                return categoryResult;

            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }
    }
}
