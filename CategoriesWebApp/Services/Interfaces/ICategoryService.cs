﻿using CategoriesWebApp.Models.ViewModels;
using System.Threading.Tasks;

namespace CategoriesWebApp.Services.Interfaces
{
    public interface ICategoryService
    {

        Task<bool> Create(CreateCategoryViewModel model);
        Task<bool> Edit(CreateCategoryViewModel model);
        Task<bool> Delete(int id);
    }
}
