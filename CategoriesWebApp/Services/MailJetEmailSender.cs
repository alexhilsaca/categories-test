﻿using CategoriesWebApp.Models;
using Mailjet.Client;
using Mailjet.Client.Resources;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace CategoriesWebApp.Services
{
    public class MailJetEmailSender : IEmailSender
    {
        public readonly IConfiguration _configuration;
        public MailJetOptions _mailJetOptions;

        public MailJetEmailSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            _mailJetOptions = _configuration.GetSection("MailJet").Get<MailJetOptions>();
            var client = new MailjetClient(_mailJetOptions.ApiKey, _mailJetOptions.SecretKey);
            var request = new MailjetRequest
            {
                Resource = Send.Resource,
            }.Property(Send.FromEmail, "alexhilsaca@protonmail.com")
                .Property(Send.FromName, "Categories Web App")
                .Property(Send.Subject, subject)

                .Property(Send.HtmlPart, htmlMessage)
                .Property(Send.Recipients, new JArray {
                    new JObject {
                        {"Email", email}
                    }
                });

            var response = await client.PostAsync(request);



        }
    }
}
