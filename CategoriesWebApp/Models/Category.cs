﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CategoriesWebApp.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Action { get; set; }
        [Required]
        public string Controller { get; set; }

        [Required]
        public IdentityRole Role { get; set; }

        public List<SubCategory> SubCategories { get; set; }


        public string RoleId { get; set; }

    }
}
