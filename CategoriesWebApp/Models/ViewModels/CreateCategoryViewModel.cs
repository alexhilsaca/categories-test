﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CategoriesWebApp.Models.ViewModels
{
    public class CreateCategoryViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Action { get; set; }
        [Required]
        public string Controller { get; set; }

        [Required]
        public string RoleSelected { get; set; }

        public List<SelectListItem> Roles { get; set; }

        public List<SubCategoryViewModel> SubCategories { get; set; }
        public int Index { get; set; }
    }
}
