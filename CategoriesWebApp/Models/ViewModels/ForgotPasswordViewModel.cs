﻿using System.ComponentModel.DataAnnotations;

namespace CategoriesWebApp.Models.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
