﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace CategoriesWebApp.Models.ViewModels
{
    public class UserViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string RoleSelected { get; set; }
        public List<SelectListItem> Roles { get; set; }
    }
}
