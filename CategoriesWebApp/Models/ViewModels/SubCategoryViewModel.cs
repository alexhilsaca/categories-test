﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CategoriesWebApp.Models.ViewModels
{
    public class SubCategoryViewModel
    {

        public int? Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Action { get; set; }

        [Required]
        public int CategoryId { get; set; }


        [Required]
        public string RoleSelected { get; set; }

        public List<SelectListItem> Roles { get; set; }

        public CreateCategoryViewModel Category { get; set; }
    }
}
