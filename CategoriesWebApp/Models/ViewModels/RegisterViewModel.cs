﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CategoriesWebApp.Models.ViewModels
{
    public class RegisterViewModel
    {


        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }


        [Required]
        public string Name { get; set; }

        [Required]
        public string RoleSelected { get; set; }


        public List<SelectListItem> Roles { get; set; }
    }
}
